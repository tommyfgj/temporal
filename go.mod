module go.temporal.io/server

go 1.16

require (
	cloud.google.com/go/storage v1.15.0
	github.com/aws/aws-sdk-go v1.38.26
	github.com/blang/semver/v4 v4.0.0
	github.com/cactus/go-statsd-client/statsd v0.0.0-20200423205355-cb0885a1018c
	github.com/cch123/elasticsql v1.0.1
	github.com/dgrijalva/jwt-go/v4 v4.0.0-preview1
	github.com/dgryski/go-farm v0.0.0-20200201041132-a6ae2369ad13
	github.com/emirpasic/gods v0.0.0-20190624094223-e689965507ab
	github.com/fatih/color v1.10.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gocql/gocql v0.0.0-20210621133426-d83b80dfb480
	github.com/gogo/protobuf v1.3.2
	github.com/gogo/status v1.1.0
	github.com/golang/mock v1.5.0
	github.com/google/uuid v1.2.0
	github.com/gorilla/websocket v1.4.2
	github.com/hashicorp/go-hclog v0.16.1
	github.com/hashicorp/go-plugin v1.4.1
	github.com/iancoleman/strcase v0.1.3
	github.com/jmoiron/sqlx v1.3.4
	github.com/jonboulle/clockwork v0.2.2
	github.com/lib/pq v1.10.1
	github.com/m3db/prometheus_client_golang v0.8.1
	github.com/olekukonko/tablewriter v0.0.5
	github.com/olivere/elastic v6.2.35+incompatible
	github.com/olivere/elastic/v7 v7.0.24
	github.com/pborman/uuid v1.2.1
	github.com/robfig/cron v1.2.0
	github.com/stretchr/testify v1.7.0
	github.com/temporalio/ringpop-go v0.0.0-20200708034907-1e016ebb537a
	github.com/temporalio/tctl v0.0.0-20210722235117-a400f112efa4
	github.com/uber-go/tally v3.3.17+incompatible
	github.com/uber/tchannel-go v1.21.1
	github.com/urfave/cli v1.22.5
	github.com/urfave/cli/v2 v2.3.0
	github.com/valyala/fastjson v1.6.3
	github.com/xwb1989/sqlparser v0.0.0-20180606152119-120387863bf2
	go.opentelemetry.io/otel v0.15.0
	go.opentelemetry.io/otel/exporters/metric/prometheus v0.15.0
	go.temporal.io/api v1.4.1-0.20210622200201-edd2d5680749
	go.temporal.io/sdk v1.8.0
	go.temporal.io/version v0.0.0-20201015012359-4d3bb966d193
	go.uber.org/atomic v1.7.0
	go.uber.org/multierr v1.6.0
	go.uber.org/zap v1.16.0
	golang.org/x/oauth2 v0.0.0-20210413134643-5e61552d6c78
	golang.org/x/time v0.0.0-20210220033141-f8bda1e9f3ba
	google.golang.org/api v0.45.0
	google.golang.org/grpc v1.38.0
	google.golang.org/grpc/examples v0.0.0-20210426212906-52a707c0dafe
	gopkg.in/square/go-jose.v2 v2.5.1
	gopkg.in/validator.v2 v2.0.0-20210331031555-b37d688a7fb0
	gopkg.in/yaml.v2 v2.4.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
